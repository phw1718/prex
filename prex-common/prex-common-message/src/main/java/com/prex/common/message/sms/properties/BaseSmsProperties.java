package com.prex.common.message.sms.properties;

/**
 * @Classname SocialProperties
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-06 14:50
 * @Version 1.0
 */
public abstract class BaseSmsProperties {

    private String accessKeyId;
    private String accessKeySecret;

    public BaseSmsProperties() {
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }
}
