package com.prex.base.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.prex.base.api.entity.SysDictItem;

/**
 * @Classname ISysDictItemService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-02 18:07
 * @Version 1.0
 */
public interface ISysDictItemService extends IService<SysDictItem> {

//    IPage<SysDictItem> selectDictItemListByDictId();
}
