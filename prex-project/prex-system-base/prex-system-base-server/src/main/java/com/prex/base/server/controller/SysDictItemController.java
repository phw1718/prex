package com.prex.base.server.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.prex.base.api.entity.SysDictItem;
import com.prex.base.server.service.ISysDictItemService;
import com.prex.common.core.utils.R;
import com.prex.common.log.annotation.SysOperateLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @Classname SysDictItemController
 * @Description
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-02 18:14
 * @Version 1.0
 */
@Api(description = "字典详情模块")
@RestController
@RequestMapping("/dictItem")
public class SysDictItemController {


    @Autowired
    private ISysDictItemService dictItemService;

    /**
     * 分页查询字典详情内容
     *
     * @param page        分页对象
     * @param sysDictItem
     * @return
     */
    @ApiOperation("查询字典详情集合")
    @SysOperateLog(descrption = "查询字典详情集合")
    @GetMapping
    public R getDictItemPage(Page page, SysDictItem sysDictItem) {
        return R.ok(dictItemService.page(page, Wrappers.query(sysDictItem)));
    }

    /**
     * @param sysDictItem
     * @return
     */
    @ApiOperation("添加字典详情")
    @SysOperateLog(descrption = "添加字典详情")
    @PreAuthorize("hasAuthority('sys:dictItem:add')")
    @PostMapping
    public R add(@RequestBody SysDictItem sysDictItem) {
        return R.ok(dictItemService.save(sysDictItem));
    }

    /**
     * @param sysDictItem
     * @return
     */
    @ApiOperation("更新字典详情")
    @SysOperateLog(descrption = "更新字典详情")
    @PreAuthorize("hasAuthority('sys:dictItem:edit')")
    @PutMapping
    public R update(@RequestBody SysDictItem sysDictItem) {
        return R.ok(dictItemService.updateById(sysDictItem));
    }

    /**
     * @param id
     * @return
     */
    @ApiOperation("删除字典详情")
    @SysOperateLog(descrption = "删除字典详情")
    @PreAuthorize("hasAuthority('sys:dictItem:del')")
    @DeleteMapping("/{id}")
    public R delete(@PathVariable("id") String id) {
        return R.ok(dictItemService.removeById(id));
    }


}
